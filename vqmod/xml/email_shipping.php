<modification>
	<file name="catalog/view/theme/*/template/checkout/confirm.tpl">
		<operation>
			<search position="replace" offset="7"><![CDATA[<tfoot>]]></search>
			<add></add>
		</operation>
		<operation>
			<search position="replace"><![CDATA[<td class="price"><?php echo $column_price; ?></td>]]></search>
			<add></add>
		</operation>
		<operation>
			<search position="replace"><![CDATA[<td class="total"><?php echo $column_total; ?></td>]]></search>
			<add></add>
		</operation>
		<operation>
			<search position="replace"><![CDATA[<td class="price"><?php echo $product['price']; ?></td>]]></search>
			<add></add>
		</operation>
		<operation>
			<search position="replace"><![CDATA[<td class="total"><?php echo $product['total']; ?></td>]]></search>
			<add></add>
		</operation>
	</file>
	<file name="catalog/view/theme/*/template/account/order_info.tpl">
		<operation>
			<search position="replace" offset="7"><![CDATA[<?php foreach ($totals as $total) { ?>]]></search>
			<add></add>
		</operation>
	</file>
	<file name="catalog/view/theme/*/template/account/order_list.tpl">
		<operation>
			<search position="replace"><![CDATA[<b><?php echo $text_total; ?></b> <?php echo $order['total']; ?>]]></search>
			<add></add>
		</operation>
	</file>
</modification>