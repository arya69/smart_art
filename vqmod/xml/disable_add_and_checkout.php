<modification>
	<file name="catalog/view/theme/*/template/product/product.tpl">
		<operation>
			<search position="replace"><![CDATA[<input type="button" value="Buy" id="button-cart" class="button btn btn-danger" />]]></search>
			<add></add>
		</operation>
	</file>
	<file name="catalog/controller/checkout/cart.php">
		<operation>
			<search position="after"><![CDATA[public function index() {]]></search>
			<add><![CDATA[$this->redirect($this->url->link('error/not_found'));]]></add>
		</operation>
	</file>
	<file name="catalog/controller/checkout/checkout.php">
		<operation>
			<search position="after"><![CDATA[public function index() {]]></search>
			<add><![CDATA[$this->redirect($this->url->link('error/not_found'));]]></add>
		</operation>
	</file>
</modification>