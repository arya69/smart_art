<?php
// Heading
$_['heading_title'] = 'Your Account Has Been Created!';

// Text
$_['text_message']  = '<p>Congratulations! Your new account has been successfully created!</p> <p>You can now take advantage of member privileges to enhance your online shopping experience with us.</p> <p>If you have ANY questions about the operation of this online shop, please email us.</p> <p>A confirmation has been sent to the provided email address. If you have not received it within the hour, please <a href="%s">contact us</a>.</p>';
$_['text_approval'] = '<p>Thank you for registering with %s!</p><p>You will be notified by email once your account has been activated.</p><p>If you have any questions about the operation of the on-line shop, please contact us at  <a href="%s">info@smartartdirect.com</a>.</p><p>Thank You</p>';
$_['text_account']  = 'Account';
$_['text_success']  = 'Success';
?>