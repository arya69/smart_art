<div class="focus-top clearfix inf-img">
	<div class="non-col-md-12 box category-bx">
		<div class="non-row">
			<div class="category-list-line clearfix box-category">
				<ul class="cat-gall col-md-12 text-right visible-lg hidden-md hidden-sm hidden-xs">
				    <?php
					if (!empty($categories_modul)) {
						foreach ($categories_modul as $category) { 
					?>
				        <li class="cat-module">
		        		<?php if ($category['category_id'] == $category_id) { ?>
				        	<a  class="btn btn-monza-art" href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
				        <?php } else { ?>
				        	<a  class="btn btn-monza-art" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
				        <?php } ?>
		        		</li>
					<?php 
						}
					}
					?>
			    </ul>
			    <?php /*
			    <div class="dropdown cat-dropdown cat-gall hidden-lg visible-md visible-sm visible-xs">
					<button class="btn btn-monza-art dropdown-toggle" type="button" id="category-dr-list" data-toggle="dropdown" aria-expanded="true">
						Category
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu cat-dr-list" role="menu" aria-labelledby="category-dr-list">
						<?php
						if (!empty($categories_modul)) {
							foreach ($categories_modul as $category) { 
						?>
					        <li class="cat-module" role="presentation">
			        		<?php if ($category['category_id'] == $category_id) { ?>
					        	<a role="menuitem" tabindex="-1" class="btn-monza-art" href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
					        <?php } else { ?>
					        	<a role="menuitem" tabindex="-1" class="btn-monza-art" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
					        <?php } ?>
			        		</li>
						<?php 
							}
						}
						?>
					</ul>
				</div>
				*/ ?>
				<div class="cat-drop-down">
					<ul class="cat-gall cat-module-dr col-md-12 hidden-lg visible-md visible-sm visible-xs">
						<li class="btn-monza-art">
							Category
							<!-- <span class="caret"></span> -->
							<i class="fa fa-caret-down"></i>
							<ul>
							    <?php
								if (!empty($categories_modul)) {
									foreach ($categories_modul as $category) { 
								?>
							        <li>
					        		<?php if ($category['category_id'] == $category_id) { ?>
							        	<a  class="btn-monza-art" href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
							        <?php } else { ?>
							        	<a  class="btn-monza-art" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
							        <?php } ?>
					        		</li>
								<?php 
									}
								}
								?>
							</ul>
						</li>
				    </ul>
				</div>
			</div>
		</div>
	</div>
</div>