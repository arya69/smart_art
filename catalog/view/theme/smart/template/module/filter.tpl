<div class="box filter-product">
  <div class="box-heading"><?php //echo $heading_title; ?></div>
  <div class="box-content">
    <ul class="box-filter inline-filter">
      <?php foreach ($filter_groups as $filter_group) { ?>
      <li class="expanded">
        <a class="on btn-monza-art">
          <span id="filter-group<?php echo $filter_group['filter_group_id']; ?>"><?php echo $filter_group['name']; ?></span>
          <i class="fa fa-caret-down right"></i>
        </a>
        <ul class="submuneu">
          <?php foreach ($filter_group['filter'] as $filter) { ?>
            <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
            <li>
              <input type="checkbox" value="<?php echo $filter['filter_id']; ?>" id="filter<?php echo $filter['filter_id']; ?>" checked="checked" />
              <label for="filter<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></label>
            </li>
            <?php } else { ?>
            <li>
              <input type="checkbox" value="<?php echo $filter['filter_id']; ?>" id="filter<?php echo $filter['filter_id']; ?>" />
              <label for="filter<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></label>
            </li>
            <?php } ?>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
    <a id="button-filter" class="btn-monza-art btn-search"><?php echo $button_filter; ?>&nbsp;&nbsp;<i class="fa fa-search"></i></a>
    </ul>
  </div>
</div>
<script type="text/javascript"><!--
$('#button-filter').bind('click', function() {
	filter = [];
	
	$('.box-filter input[type=\'checkbox\']:checked').each(function(element) {
		filter.push(this.value);
	});
	
	location = '<?php echo $action; ?>&filter=' + filter.join(',');
});
//--></script> 
