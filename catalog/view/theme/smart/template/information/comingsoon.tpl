<link href='http://fonts.googleapis.com/css?family=Oswald:700,300|PT+Sans+Caption' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Crete+Round|Parisienne' rel='stylesheet' type='text/css'>
<?php echo $header; ?>
<?php echo $column_left; ?>
<?php echo $column_right; ?>
<content>
	<div id="container" class="container comming-soon">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content">
					<?php echo $content_top; ?>
					<div class="content-top-det clearfix">
						<div class="focus clearfix">
							<div class="breadcrumb">
									<?php foreach ($breadcrumbs as $breadcrumb) { ?>
									<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
									<?php } ?>
							</div>
							<div class="col-md-12">
								<div id="coming-soon">
									<div class="watch"></div>
									<div class="rest">
										<div class="clear-top"></div>
										<h2 class="soon"><span>our shop is</span><br/>Coming Soon</h2>
									</div>
								</div>
							</div>
							<?php echo $content_bottom; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?>