<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
<div class="breadcrumb">
		<?php /*foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
			<?php } */?>
				<!--<h1><?php //echo $heading_title; ?></h1>-->
					<?php /*echo $description; */?>
				<!--<div class="right"><a href="<?php /*echo $continue; ?>" class="button"><?php echo $button_continue; */?></a></div>-->
	
		<?php echo $content_bottom; ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="content">
						<ul class="nav nav-tabs ul-tab-art">
							<li class="li-tab-art"><a href="#stories" data-toggle="tab">Stories</a></li>
							<li class="li-tab-art active"><a href="#work" data-toggle="tab">Art Work</a></li>
							<li class="li-tab-art"><a href="#product" data-toggle="tab">Product</a></li>
						</ul>
						<div class="tab-content clearfix">
							<div class="tab-pane clearfix" id="stories">
								
							</div>
							<div class="tab-pane clearfix active" id="work">
								<div class="loop-gall col-md-3 text-center">
									<a href="#" class="link-det-img">
										<img src="catalog/view/theme/smart/image/img/art_1.jpg" class="img-det">
										<div class="shrt-desc-img">Interior Designers / Architects</div>
									</a>
								</div>
								<div class="loop-gall col-md-3 text-center">
									<a href="#" class="link-det-img">
										<img src="catalog/view/theme/smart/image/img/art_2.jpg" class="img-det">
										<div class="shrt-desc-img">Hotels / Resorts</div>
									</a>
								</div>
								<div class="loop-gall col-md-3 text-center">
									<a href="#" class="link-det-img">
										<img src="catalog/view/theme/smart/image/img/art_3.jpg" class="img-det">
										<div class="shrt-desc-img">Aged Care / Senior Living</div>
									</a>
								</div>
								<div class="loop-gall col-md-3 text-center">
									<a href="#" class="link-det-img">
										<img src="catalog/view/theme/smart/image/img/art_4.jpg" class="img-det">
										<div class="shrt-desc-img">Office Fit-outs</div>
									</a>
								</div>
								<div class="loop-gall col-md-3 text-center">
									<a href="#" class="link-det-img">
										<img src="catalog/view/theme/smart/image/img/art_5.jpg" class="img-det">
										<div class="shrt-desc-img">Corporate / Strata</div>
									</a>
								</div>
								<div class="loop-gall col-md-3 text-center">
									<a href="#" class="link-det-img">
										<img src="catalog/view/theme/smart/image/img/art_6.jpg" class="img-det">
										<div class="shrt-desc-img">Residential</div>
									</a>
								</div>
								<div class="loop-gall col-md-3 text-center">
									<a href="#" class="link-det-img">
										<img src="catalog/view/theme/smart/image/img/art_7.jpg" class="img-det">
										<div class="shrt-desc-img">Hospitality</div>
									</a>
								</div>
								<div class="loop-gall col-md-3 text-center">
									<a href="#" class="link-det-img">
										<img src="catalog/view/theme/smart/image/img/art_8.jpg" class="img-det">
										<div class="shrt-desc-img">Retailer Buyers</div>
									</a>
								</div>
							</div>
							<div class="tab-pane clearfix" id="product">
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer; ?>