<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div><?php echo $content_top; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div class="slides">
					<div id="carousel-example-generic" class="slider carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<div class="item active">
								<img class="slider" src="catalog/view/theme/smart/stylesheet/img/slide_1.jpg" alt="...">
								<!-- <div class="container "> -->
									<div class="carousel-caption carousel-art">
										<h1 class="title">Smart Art Direct Global</h1>
										<span class="desc">Specialists in the design and production of affordable, hand-painted artwork</span>
									</div>
								<!-- </div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $content_bottom; ?></div>
<?php echo $footer; ?>