<footer>
	<div id="footer">
		<div class="container">
			<div class="col-md-12 col-xs-12">
				<div class="row">		
					<div class="main-footer clearfix">
						<div class="col-md-6">
							<ul class="social-media">
								<li>
									<a href="https://www.facebook.com/pages/Smart-Art-Direct-Global/125556034121294">
									<img src="catalog/view/theme/smart/stylesheet/img/fb.png"></a>
								</li>
								<li>
									<a href="https://instagram.com/smart.art.direct/">
									<img src="catalog/view/theme/smart/stylesheet/img/insta.png"></a>
								</li>
								<li>
									<a href="http://www.pinterest.com/SmartArtDirect/">
									<img src="catalog/view/theme/smart/stylesheet/img/pinterest.png"></a>
								</li>
								<li>
									<a href="">
									<img src="catalog/view/theme/smart/stylesheet/img/paypal.png"></a>
								</li>
								<!-- <li>
									<a href="#">
									<img src="catalog/view/theme/smart/stylesheet/img/tw.png"></a>
								</li> -->
								<li>Copyright Smart Art Direct. All rights reserved <?php echo date('Y'); ?>.</li>
							</ul>
						</div>
						<div class="col-md-6">
							<ul class="footer-nav">
								<li><a href="http://red.infomesta.net/~smartart/frequently-asked-questions/" class="menu-image-title-">FAQ</a></li>
								<li><a href="http://red.infomesta.net/~smartart/sitemap/" class="menu-image-title-">SITEMAP</a></li>
								<li id="last-footer"><a href="http://red.infomesta.net/~smartart/contact-us/" class="menu-image-title-">CONTACT</a></li>
							</ul>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</footer>
</body></html>