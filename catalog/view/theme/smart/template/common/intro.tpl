<?php echo $introheader; ?>
<content>
	<div class="slides">
		<div id="carousel-example-generic" class="slider carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img class="slider" src="catalog/view/theme/smart/stylesheet/img/slide_1.jpg" alt="...">
					<div class="container ">
						<div class="carousel-caption carousel-art">
							<h1 class="title">Smart Art Direct Global</h1>
							<span class="desc">Specialists in the design and production of affordable, hand-painted artwork</span>
						</div>
					</div>
				</div>
				<div class="item">
					<img class="slider" src="catalog/view/theme/smart/stylesheet/img/slide_2.jpg" alt="...">
					<div class="container ">
						<div class="carousel-caption carousel-art">
							<h1 class="title">Smart Art Direct Global</h1>
							<span class="desc">Specialists in the design and production of affordable, hand-painted artwork</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?>