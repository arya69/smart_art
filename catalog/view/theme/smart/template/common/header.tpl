<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
	<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<?php } ?>
	<?php if ($icon) { ?>
	<link href="<?php echo $icon; ?>" rel="icon" />
	<?php } ?>
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>
	<!-- <link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/stylesheet.css" /> -->
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/css/dist/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/css/dist/app.css">
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/css/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/css/style.css">
	<?php foreach ($styles as $style) { ?>
	<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
	<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
	<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
	<script type="text/javascript" src="catalog/view/theme/smart/stylesheet/js/modernizr.js"></script>
	<?php foreach ($scripts as $script) { ?>
	<script type="text/javascript" src="<?php echo $script; ?>"></script>
	<?php } ?>
	<!-- load bootstrap js -->
	<script type="text/javascript" src="catalog/view/theme/smart/stylesheet/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="catalog/view/theme/smart/stylesheet/js/main.js"></script>
	<!--[if IE 7]> 
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/ie7.css" />
	<![endif]-->
	<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/ie6.css" />
	<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
	<script type="text/javascript">
	DD_belatedPNG.fix('#logo img');
	</script>
	<![endif]-->
	<?php if ($stores) { ?>
	<script type="text/javascript"><!--
	$(document).ready(function() {
	<?php foreach ($stores as $store) { ?>
	$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
	<?php } ?>
	});
	//--></script>
	<?php } ?>
	<?php echo $google_analytics; ?>
	
	<script type="text/javascript"><!--
	$(document).ready(function() {
		// $('.banner').unslider();
		$('#slideshow').nivoSlider({
			effect: 'random',
			controlNav: false,
		});
	});
	--></script>
	</head>
	<body id="catalog">
	<div id="container">
	<header>
		<div class="header">
			<?php /*
			<div class="menu-top-art clearfix red">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="row">
								<div class="col-md-6 col-md-push-6">
									<div class="col-md-6 login">
										<div class="row">										
											<?php if (!$logged) { ?>
												<a href="index.php?route=account/login">Corporate Login</a>
											<?php } else { ?>
												<?php echo $text_logged; ?>
											<?php } ?>											
										</div>
									</div>
									<div class="col-md-6" id="search">
										<div class="row">
											<div class="form-inline pull-right">
												<div class="form-group">
													<input class="form-control" type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
													<button type="submit" class="btn btn-default btn-search-art">
														<i class="fa fa-search"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-md-pull-6 cart-header">
									<?php echo $cart; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			*/ ?>
			<div class="head-bg-art clearfix">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 clr-art-wht">
							<div class="row">
								<div class="container">
									<div class="row">
										<div class="col-md-12">
											<div class="row">												
													<div class="col-md-4">
														<div class="pull-left logo-art text-left menu-top-art">
															<?php if ($logo) { ?>
																<div id="logo">
																	<a href="http://red.infomesta.net/~smartart/">
																		<img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
																	</a>
																</div>
															<?php } ?>
														</div>								
													</div>
													<div class="col-md-8">
														<div class="login-top">
															<div class="col-md-12">
																<div class="row">
																	<div class="col-md-5 text-right login-link">
																		<?php if (!$logged) { ?>
																			<a href="index.php?route=account/login" >Sign In</a>
																		<?php } else { ?>
																			<?php echo $text_logged; ?>
																		<?php } ?>
																	</div>
																	<div class="col-md-4 top-search">
																		<div class="col-sm-10 col-xs-10 search-input-fld">
																			<div class="row">
																				<input class="form-control large-cari" type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
																			</div>
																		</div>
																		<div class="col-sm-2 col-xs-2 search-input-btn">
																			<div class="row">
																				<button type="submit" class="btn btn-default non-btn-search-art">
																					<i class="fa fa-search"></i>
																				</button>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-3 cart-on-top">
																		<?php echo $cart; ?>
																	</div>
																</div>
															</div>															
														</div>
													</div>			
											</div>						
										</div>						
									</div>						
								</div>
							</div>
						</div>
						<div class="col-md-12 clr-art-red">
							<div class="row">
								<div class="col-md-6 hidden-xs hidden-sm">
									&nbsp;
								</div>
								<div class="col-md-6">
									<div class="row hidden-xs hidden-sm">
										<div class="container">
											<button type="button" class="navbar-toggle menu-mobile" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">   
												<span class="sr-only">Toggle navigation</span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</button>
											<a class="navbar-brand visible-xs" style="color:#fff;" href="#">Menu</a>
										</div>
										<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
											<div class="nav-collapse clearfix">
												<ul class="nav-art pull-left nav navbar-nav navbar-inner">
													<?php
													if (!empty($categories)) {
													foreach ($categories as $category) {
														if ($category['children']) {
													?>
														<li class="dropdown <?php echo ( $pageURL==$category['href'] ? 'actives' : '' ); ?>">
															<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?> <?php if (!empty($category['tagmenu'])) { echo "<br/>".$category['tagmenu'];} ?></a>														
													<?php
															for ($i = 0; $i < count($category['children']);) {
																echo '<ul class="dropdown-menu" role="menu">';
																$j = $i + ceil(count($category['children']) / $category['column']);
																for (; $i < $j; $i++) {
													?>
																	<li class="<?php echo ( $pageURL==$category['children'][$i]['href'] ? 'actives' : '' ); ?>">
																		<a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?> <?php if (!empty($category['children'][$i]['tagmenu'])) { echo "<br/>".$category['children'][$i]['tagmenu'];} ?></a>
																	</li>
													<?php
																}
																echo '</ul>';
															}
													?>
														</li>
													<?php
														} else {
													?>
														<li class="<?php echo ( $pageURL==$category['href'] ? 'actives' : '' ); ?>">
															<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?> <?php if (!empty($category['tagmenu'])) { echo "<br/>".$category['tagmenu'];} ?></a>
														</li>
													<?php
														}
													}
													}
													?>													
												</ul>
											</div>
										</div>										
									</div>
									<div class="row visible-xs visible-sm">						
										<select id="menu-select">
											<option value="" class="blank">– MENU –</option>
											<?php
												if (!empty($categories)) {
													foreach ($categories as $category) {
														if ($category['children']) {
												?>
														<option value="<?php echo $category['href']; ?>">
															<?php echo $category['name']; ?> 
															<?php if (!empty($category['tagmenu'])) { echo "<br/>".$category['tagmenu'];} ?>
														</option>														
												<?php
														for ($i = 0; $i < count($category['children']);) {
															$j = $i + ceil(count($category['children']) / $category['column']);
															for (; $i < $j; $i++) {
												?>																	
																<option value="<?php echo $category['children'][$i]['href']; ?>">
																	- <?php echo $category['children'][$i]['name']; ?> 
																	<?php if (!empty($category['children'][$i]['tagmenu'])) { echo "<br/>".$category['children'][$i]['tagmenu'];} ?>
																</option>																	
												<?php
															}
														}
												?>
												<?php
													} else {
												?>
													<option value="<?php echo $category['href']; ?>">
														<?php echo $category['name']; ?> 
														<?php if (!empty($category['tagmenu'])) { echo "<br/>".$category['tagmenu'];} ?>
													</option>
												<?php
														}
													}
												}
												?>	
										</select>
										<script>
										    $(function(){
										      $('#menu-select').bind('change', function () {
										          var url = $(this).val();
										          if (url) {
										              window.location = url;
										          }
										          return false;
										      });
										    });
										</script>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php  
			if (!empty($this->request->get['route'])) {
				if ("product/category"==$this->request->get['route']) {
			?>
			<div class="center-img-art">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="container">
								<div class="img-header">
									<?php 
									if (!empty($image_banner)) {
										echo '<img src="'.$image_banner.'" class="top-img">';
									} else {
									?>
										<img src="<?php echo $base;?>image/bg_top_data.jpg" class="top-img">
									<?php } ?>
								</div>
								<?php /*
								<div class="img-header-non slider-wrapper theme-default">
									<!-- <img src="http://blacky:81/smartart_com/image/bg_top.jpg" class="top-img"> -->									
									<div id="slideshow" class="nivoSlider">
										<?php foreach ($banners as $banner) { ?>
										<?php if ($banner['link']) { ?>
											<a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="top-img" /></a>
										<?php } else { ?>
											<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="top-img" />
										<?php } ?>
										<?php } ?>
									</div>													
								</div>
								*/ ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
				}
			}
			?>
		</div>
	</header>
	<br clear="all">
	
	<!-- <content> -->
	<div  class="container">
		<div class="row">
			<div class= "col-md-12 col-xs-12 clearfix">
				<div class="container">
					<?php if ($error) { ?>
						<div class="warning"><?php echo $error ?><img src="catalog/view/theme/smart/image/close.png" alt="" class="close" />
						</div>
					<?php } ?>
					<div id="notification"></div>
				</div>