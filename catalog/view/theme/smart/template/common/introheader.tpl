<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
	<head>
	<meta charset="UTF-8" />
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<?php } ?>
	<?php if ($icon) { ?>
	<link href="<?php echo $icon; ?>" rel="icon" />
	<?php } ?>
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>
	<!-- <link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/stylesheet.css" /> -->
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/css/dist/app.css">
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/css/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/css/style.css">
	<?php foreach ($styles as $style) { ?>
	<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
	<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
	<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
	<script type="text/javascript" src="catalog/view/theme/smart/stylesheet/js/modernizr.js"></script>
	<?php foreach ($scripts as $script) { ?>
	<script type="text/javascript" src="<?php echo $script; ?>"></script>
	<?php } ?>
	<!-- load bootstrap js -->
	<script type="text/javascript" src="catalog/view/theme/smart/stylesheet/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="catalog/view/theme/smart/stylesheet/js/main.js"></script>
	<!--[if IE 7]> 
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/ie7.css" />
	<![endif]-->
	<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="catalog/view/theme/smart/stylesheet/ie6.css" />
	<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
	<script type="text/javascript">
	DD_belatedPNG.fix('#logo img');
	</script>
	<![endif]-->
	<?php if ($stores) { ?>
	<script type="text/javascript"><!--
	$(document).ready(function() {
	<?php foreach ($stores as $store) { ?>
	$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
	<?php } ?>
	});
	//--></script>
	<?php } ?>
	<?php echo $google_analytics; ?>
	<script>
       $('.carousel').carousel({
         interval: 500
       });
    </script>
	</head>
	<body id="catalog">
	<div id="container">
	<header>
		<div class="header">
			<div class="menu-top-art clearfix red">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="row">
								<div class="col-md-6 col-md-push-6">
									<div class="col-md-6 login">
										<div class="row">										
											<?php if (!$logged) { ?>
												<a href="index.php?route=account/login">Corporate Login</a>
											<?php } else { ?>
												<?php echo $text_logged; ?>
											<?php } ?>											
										</div>
									</div>
									<div class="col-md-6" id="search">
										<div class="row">
											<div class="form-inline pull-right">
												<div class="form-group">
													<input class="form-control" type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
													<button type="submit" class="btn btn-default btn-search-art">
														<i class="fa fa-search"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-md-pull-6">
									<?php echo $cart; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="head-bg-art clearfix">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-4 clr-art-wht">
							<div class="row">
								<div class="logo-art text-right">
									<?php if ($logo) { ?>
										<div id="logo"><a href="http://red.infomesta.net/~smartart/"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="col-md-8 clr-art-red">
							<div class="row">
								<div class="col-md-11">
									<div class="row">
										<div class="container">
											<button type="button" class="navbar-toggle menu-mobile" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">   
												<span class="sr-only">Toggle navigation</span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</button>
											<a class="navbar-brand visible-xs" style="color:#fff;" href="#">Menu</a>
										</div>
										<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
											<div class="nav-collapse clearfix">
												<ul class="nav-art pull-right nav navbar-nav navbar-inner">
													<?php
													foreach ($categories as $category) {
														if ($category['children']) {
													?>
														<li class="dropdown">
															<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>														
													<?php
															for ($i = 0; $i < count($category['children']);) {
																echo '<ul class="dropdown-menu" role="menu">';
																$j = $i + ceil(count($category['children']) / $category['column']);
																for (; $i < $j; $i++) {
													?>
																	<li>
																		<a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a>
																	</li>
													<?php
																}
																echo '</ul>';
															}
													?>
														</li>
													<?php
														} else {
													?>
														<li>
															<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
														</li>
													<?php
														}
													}
													?>													
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-1">
									&nbsp;
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>