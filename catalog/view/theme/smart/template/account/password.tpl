<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content"><?php echo $content_top; ?>
					<div class="content-top-det clearfix">
						<div class="focus clearfix">
							<div class="breadcrumb">
								<?php foreach ($breadcrumbs as $breadcrumb) { ?>
								<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
								<?php } ?>
							</div>
							<h1><?php echo $heading_title; ?></h1>
							<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
								<h2><?php echo $text_password; ?></h2>
								<div class="content flip-scroll">
									<table class="table table-striped">
										<tr>
											<td><span class="required">*</span> <?php echo $entry_password; ?></td>
											<td><input type="password" class="form-control" name="password" value="<?php echo $password; ?>" />
												<?php if ($error_password) { ?>
												<span class="error"><?php echo $error_password; ?></span>
												<?php } ?></td>
										</tr>
										<tr>
											<td><span class="required">*</span> <?php echo $entry_confirm; ?></td>
											<td><input type="password" class="form-control" name="confirm" value="<?php echo $confirm; ?>" />
												<?php if ($error_confirm) { ?>
												<span class="error"><?php echo $error_confirm; ?></span>
												<?php } ?></td>
										</tr>
									</table>
								</div>
								<br>
								<div class="buttons">
									<div class="right">
										<a href="<?php echo $back; ?>" class="button btn btn-primary"><?php echo $button_back; ?></a>
										<input type="submit" class="btn btn-primary" value="<?php echo $button_continue; ?>" class="button" />
									</div>
								</div>
							</form>
							<?php echo $content_bottom; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?>