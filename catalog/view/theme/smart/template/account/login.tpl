<?php echo $header; ?>
<div class="container">
	<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content"><?php echo $content_top; ?>
					<div class="content-top-det clearfix">
						<div class="focus clearfix">
							<div class="breadcrumb">
								<div class="col-md-12">
									<?php foreach ($breadcrumbs as $breadcrumb) { ?>
									<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
									<?php } ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-12">
									<h1><?php echo $heading_title; ?></h1>
								</div>
								<div class="login-content">
									<!-- <div class="left"> -->
										<div class="col-md-6">
											<!-- <div class="row"> -->
												<h2><?php echo $text_new_customer; ?></h2>
												<div class="content">
													<p><b><?php echo $text_register; ?></b></p>
													<p><?php echo $text_register_account; ?></p>
													<a href="<?php echo $register; ?>" class="button btn btn-primary"><?php echo $button_continue; ?></a>
												</div>
											<!-- </div> -->
										</div>
									<!-- </div> -->
									<!-- <div class="right"> -->
										<div class="col-md-6">
											<h2><?php echo $text_returning_customer; ?></h2>
											<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
												<div class="content">
													<p><?php echo $text_i_am_returning_customer; ?></p>
													<b><?php echo $entry_email; ?></b><br />
													<input type="text" class="form-control large-field" name="email" value="<?php echo $email; ?>" />
													<br />
													<b><?php echo $entry_password; ?></b><br />
													<input type="password" class="form-control large-field" name="password" value="<?php echo $password; ?>" />
													<br />
													<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a><br />
													<br />
													<input type="submit" class="btn btn-primary" value="<?php echo $button_login; ?>" class="button" />
													<?php if ($redirect) { ?>
													<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
													<?php } ?>
												</div>
											</form>
										</div>
									<!-- </div> -->
								</div>
							</div>
							<?php echo $content_bottom; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<script type="text/javascript"><!--
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#login').submit();
	}
});
//--></script> 
<?php echo $footer; ?>