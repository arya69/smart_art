<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content"><?php echo $content_top; ?>
					<div class="content-top-det clearfix">
						<div class="focus clearfix">
							<div class="breadcrumb">
								<?php foreach ($breadcrumbs as $breadcrumb) { ?>
								<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
								<?php } ?>
							</div>
							<h1><?php echo $heading_title; ?></h1>
							<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
								<p><?php echo $text_email; ?></p>
								<h2><?php echo $text_your_email; ?></h2>
								<div class="content">
									<table class="form">
										<tr>
											<td><?php echo $entry_email; ?></td>
											<td><input type="text" class="form-control" name="email" value="" /></td>
										</tr>
									</table>
								</div>
								<br>
								<div class="buttons">
									<div class="right">
										<a href="<?php echo $back; ?>" class="button btn btn-primary"><?php echo $button_back; ?></a>
										<input type="submit" value="<?php echo $button_continue; ?>" class="button btn btn-primary" />
									</div>
								</div>
							</form>
							<?php echo $content_bottom; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?>