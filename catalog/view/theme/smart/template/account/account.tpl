<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content"><?php echo $content_top; ?>
					<div class="content-top-det clearfix">
						<div class="focus clearfix">
							<div class="breadcrumb">
								<?php foreach ($breadcrumbs as $breadcrumb) { ?>
								<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
								<?php } ?>
							</div>
							<h1><?php echo $heading_title; ?></h1>
							<h4><?php echo $text_my_account; ?></h4>
							<div class="content">
								<ul class="my-account">
									<li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
									<li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
									<li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
									<?php /*<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>*/ ?>
								</ul>
							</div>
							<h4><?php echo $text_my_orders; ?></h4>
							<div class="content">
								<ul class="my-account">
									<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
									<?php /*<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>*/ ?>
									<?php if ($reward) { ?>
									<li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
									<?php } ?>
									<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
									<li><a href="<?php echo $create_return; ?>"><?php echo $text_create_return; ?></a></li>
									<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
									<?php /*<li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>*/ ?>
								</ul>
							</div>
							<h4><?php echo $text_my_newsletter; ?></h4>
							<div class="content">
								<ul class="my-account">
									<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
								</ul>
							</div>
							<?php echo $content_bottom; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?> 