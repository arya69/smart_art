<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content"><?php echo $content_top; ?>
					<div class="content-top-det clearfix">
						<div class="focus clearfix">
							<div class="breadcrumb">
								<?php foreach ($breadcrumbs as $breadcrumb) { ?>
								<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
								<?php } ?>
							</div>
							<h1><?php echo $heading_title; ?></h1>
							<table class="table table-striped">
								<thead>
									<tr>
										<td class="left" colspan="2"><?php echo $text_order_detail; ?></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="left" style="width: 50%;"><?php if ($invoice_no) { ?>
											<b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
											<?php } ?>
											<b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
											<b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
										<td class="left" style="width: 50%;"><?php if ($payment_method) { ?>
											<b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
											<?php } ?>
											<?php if ($shipping_method) { ?>
											<b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
											<?php } ?></td>
									</tr>
								</tbody>
							</table>
							<table class="table table-striped">
								<thead>
									<tr>
										<td class="left"><?php echo $text_payment_address; ?></td>
										<?php if ($shipping_address) { ?>
										<td class="left"><?php echo $text_shipping_address; ?></td>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="left"><?php echo $payment_address; ?></td>
										<?php if ($shipping_address) { ?>
										<td class="left"><?php echo $shipping_address; ?></td>
										<?php } ?>
									</tr>
								</tbody>
							</table>
							<div class="flip-scroll">
								<table class="table table-striped">
									<thead>
										<tr>
											<td><?php echo $column_name; ?></td>
											<td><?php echo $column_model; ?></td>
											<td><?php echo $column_quantity; ?></td>
											<?php if ($shipping_method!="We will contact you by email for details") { ?>
											<td><?php echo $column_price; ?></td>
											<td><?php echo $column_total; ?></td>
											<?php } ?>
											<?php if ($products) { ?>
											<td style="width: 1px;"></td>
											<?php } ?>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($products as $product) { ?>
										<tr>
											<td><?php echo $product['name']; ?>
												<?php foreach ($product['option'] as $option) { ?>
												<br />
												&nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
												<?php } ?></td>
											<td><?php echo $product['model']; ?></td>
											<td><?php echo $product['quantity']; ?></td>
											<?php if ($shipping_method!="We will contact you by email for details") { ?>
											<td><?php echo $product['price']; ?></td>
											<td><?php echo $product['total']; ?></td>
											<?php } ?>
											<td><a href="<?php echo $product['return']; ?>"><img src="catalog/view/theme/default/image/return.png" alt="<?php echo $button_return; ?>" title="<?php echo $button_return; ?>" /></a></td>
										</tr>
										<?php } ?>
										<?php foreach ($vouchers as $voucher) { ?>
										<tr>
											<td><?php echo $voucher['description']; ?></td>
											<td></td>
											<td>1</td>
											<td><?php echo $voucher['amount']; ?></td>
											<td><?php echo $voucher['amount']; ?></td>
											<?php if ($products) { ?>
											<td></td>
											<?php } ?>
										</tr>
										<?php } ?>
									</tbody>
									<?php if ($shipping_method!="We will contact you by email for details") { ?>
									<tfoot>
										<?php foreach ($totals as $total) { ?>
										<tr>
											<td colspan="3"></td>
											<td><b><?php echo $total['title']; ?>:</b></td>
											<td><?php echo $total['text']; ?></td>
											<?php if ($products) { ?>
											<td></td>
											<?php } ?>
										</tr>
										<?php } ?>
									</tfoot>
									<?php } ?>
								</table>
							</div>
							<?php if ($comment) { ?>
							<table class="table table-striped">
								<thead>
									<tr>
										<td class="left"><?php echo $text_comment; ?></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="left"><?php echo $comment; ?></td>
									</tr>
								</tbody>
							</table>
							<?php } ?>
							<?php if ($histories) { ?>
							<h2><?php echo $text_history; ?></h2>
							<div class="flip-scroll">
								<table class="table table-striped">
									<thead>
										<tr>
											<td class="left"><?php echo $column_date_added; ?></td>
											<td class="left"><?php echo $column_status; ?></td>
											<td class="left"><?php echo $column_comment; ?></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($histories as $history) { ?>
										<tr>
											<td class="left"><?php echo $history['date_added']; ?></td>
											<td class="left"><?php echo $history['status']; ?></td>
											<td class="left"><?php echo $history['comment']; ?></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<?php } ?>
							<div class="buttons">
								<div class="right"><a href="<?php echo $continue; ?>" class="button btn btn-primary"><?php echo $button_continue; ?></a></div>
							</div>
							<?php echo $content_bottom; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?> 