<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content"><?php echo $content_top; ?>
					<div class="content-top-det clearfix">
						<div class="focus clearfix">
							<div class="breadcrumb">
								<?php foreach ($breadcrumbs as $breadcrumb) { ?>
								<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
								<?php } ?>
							</div>
							<h1><?php echo $heading_title; ?></h1>
							<h2><?php echo $text_address_book; ?></h2>
							<?php foreach ($addresses as $result) { ?>
							<div class="content">
								<table style="width: 100%;">
									<tr>
										<td><?php echo $result['address']; ?></td>
										<td style="text-align: right;">
											<a href="<?php echo $result['update']; ?>" class="button btn btn-primary btn-address-edit"><?php echo $button_edit; ?></a> &nbsp; 
											<a href="<?php echo $result['delete']; ?>" class="button btn btn-primary"><?php echo $button_delete; ?></a></td>
									</tr>
								</table>
							</div>
							<hr>
							<?php } ?>
							<br>
							<div class="buttons">
								<div class="right">
									<a href="<?php echo $back; ?>" class="button btn btn-primary"><?php echo $button_back; ?></a>
									<a href="<?php echo $insert; ?>" class="button btn btn-primary"><?php echo $button_new_address; ?></a>
								</div>
							</div>
							<?php echo $content_bottom; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?>