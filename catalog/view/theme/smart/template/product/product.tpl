<?php echo $header; ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content"><?php echo $content_top; ?>
					<div class="content-top-det clearfix">
						<div class="focus clearfix">
						<div class="row">
							<div class="col-md-10">
								<div class="row">
									<div class="list-page">
										<div class="breadcrumb">
											<?php foreach ($breadcrumbs as $breadcrumb) { ?>
											<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="row">
									<div class="shere text-right">
										<div class="addthis_default_style text-right"><a class="addthis_button_compact"><?php echo $text_share; ?></a> <a class="addthis_button_email"></a><a class="addthis_button_print"></a> <a class="addthis_button_facebook"></a> <a class="addthis_button_twitter"></a></div>
										<script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script>					 
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
					<!--<h1><?php //echo $heading_title; ?></h1>-->
					<div class="product-info">
						<?php if ($thumb || $images) { ?>
						<div class="left">
							<div class="focus clearfix">
								<div class="inf-img clearfix">
									<div class="row">
										<div class="col-md-3">
											<?php if ($thumb) { ?>
												<a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox">
												<img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" class="img-responsive"/></a>
											<?php } ?>
										</div>
										<div class="col-md-9">
											<div class="col-md-4">
												<div class="image-additional">
												<?php if ($images) { ?>
													<?php foreach ($images as $image) { ?>
														<a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox">
														<img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="img-responsive list-img"/></a>	
													<?php } ?>
												<?php } ?>
												</div>
											</div>
											<div class="col-md-8">
												<?php if (1==$available_to_buy and null!=$available_to_buy) { ?>
													<?php if ($price) { ?>
														<span class="price">
															<?php if (!$special) { ?>
																<?php echo $price; ?>
															<?php } else { ?>
																<span class="price-old">
																	<?php echo $price; ?>
																</span>
																<span class="price-new">
																	<?php echo $special; ?>
																</span>
															<?php } ?>
														</span>
													<?php } else if($info_price) { ?>
														<span class="info_price">
															<?php echo $info_price; ?>
														</span>
													<?php } ?>
												<?php } ?>
												<span class="desc-price"><?php echo $heading_title; ?></span>
												<div class="line-separator"></div>
												<?php if (1==$available_to_buy and null!=$available_to_buy) { ?>
												<div class="row">
													<div class="col-md-12">
														<div class="size form-inline" id="optData">
															<?php /*														
															<?php if ($options) { ?>
															<div class="form-group" id="optProduct">
															<label>Size: </label>
															<select name="option_data" class="form-control" onchange="option_data(<?php echo $product_id; ?>,this.value)">
																<option value=""><?php echo $text_select; ?></option>
																<?php foreach ($options as $option) { ?>
																<option value="<?php echo $option['product_option_id']; ?>">
																	<?php echo $option['name']; ?>
																</option>
																<?php } ?>
															</select>
															</div>
															<?php } ?>
															<div class="form-group" id="option-item">
																
															</div>
															*/ ?>
															
																<?php 
																if ($options) {
																	foreach ($options as $option) { 
																?>
																		<?php 
																		if ($option['type'] == 'select') {
																			if (!$all_option_kosong) {
																		?>
																			<div class="form-group">
																				<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																					<?php if ($option['required']) { ?>
																					<span class="required">*</span>
																					<?php } ?>
																					<label><?php echo $option['name']; ?>:</label>
																					<select name="option[<?php echo $option['product_option_id']; ?>]" class="form-control">
																						<option value=""><?php echo $text_select; ?></option>
																						<?php foreach ($option['option_value'] as $option_value) { ?>
																						<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
																						<?php /*if ($option_value['price']) { ?>
																						(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																						<?php } */ ?>
																						</option>
																						<?php } ?>
																					</select>
																				</div>
																			</div>
																		<?php 
																			}
																		}
																		?>
																		<?php if ($option['type'] == 'radio') { ?>
																			<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																				<?php if ($option['required']) { ?>
																				<span class="required">*</span>
																				<?php } ?>
																				<b><?php echo $option['name']; ?>:</b><br />
																				<?php foreach ($option['option_value'] as $option_value) { ?>
																				<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
																				<label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
																					<?php if ($option_value['price']) { ?>
																					(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																					<?php } ?>
																				</label>
																				<br />
																				<?php } ?>
																			</div>
																			<br />
																		<?php } ?>
																		<?php if ($option['type'] == 'checkbox') { ?>
																			<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																				<?php if ($option['required']) { ?>
																				<span class="required">*</span>
																				<?php } ?>
																				<b><?php echo $option['name']; ?>:</b><br />
																				<?php foreach ($option['option_value'] as $option_value) { ?>
																				<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
																				<label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
																					<?php if ($option_value['price']) { ?>
																					(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																					<?php } ?>
																				</label>
																				<br />
																				<?php } ?>
																			</div>
																			<br />
																		<?php } ?>
																		<?php if ($option['type'] == 'image') { ?>
																			<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																				<?php if ($option['required']) { ?>
																				<span class="required">*</span>
																				<?php } ?>
																				<b><?php echo $option['name']; ?>:</b><br />
																				<table class="option-image">
																					<?php foreach ($option['option_value'] as $option_value) { ?>
																					<tr>
																						<td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
																						<td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
																						<td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
																								<?php if ($option_value['price']) { ?>
																								(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																								<?php } ?>
																							</label></td>
																					</tr>
																					<?php } ?>
																				</table>
																			</div>
																			<br />
																		<?php } ?>
																		<?php if ($option['type'] == 'text') { ?>
																			<div class="form-group">
																				<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																					<?php if ($option['required']) { ?>
																					<span class="required">*</span>
																					<?php } ?>
																					<label><?php echo $option['name']; ?>:</label>
																					<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="form-control"/>
																				</div>
																			</div>
																		<?php } ?>
																		<?php if ($option['type'] == 'textarea') { ?>
																			<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																				<?php if ($option['required']) { ?>
																				<span class="required">*</span>
																				<?php } ?>
																				<b><?php echo $option['name']; ?>:</b><br />
																				<textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
																			</div>
																			<br />
																		<?php } ?>
																		<?php if ($option['type'] == 'file') { ?>
																			<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																				<?php if ($option['required']) { ?>
																				<span class="required">*</span>
																				<?php } ?>
																				<b><?php echo $option['name']; ?>:</b><br />
																				<input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
																				<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
																			</div>
																			<br />
																		<?php } ?>
																		<?php if ($option['type'] == 'date') { ?>
																			<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																				<?php if ($option['required']) { ?>
																				<span class="required">*</span>
																				<?php } ?>
																				<b><?php echo $option['name']; ?>:</b><br />
																				<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
																			</div>
																			<br />
																		<?php } ?>
																		<?php if ($option['type'] == 'datetime') { ?>
																			<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																				<?php if ($option['required']) { ?>
																				<span class="required">*</span>
																				<?php } ?>
																				<b><?php echo $option['name']; ?>:</b><br />
																				<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
																			</div>
																			<br />
																		<?php } ?>
																		<?php if ($option['type'] == 'time') { ?>
																			<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
																				<?php if ($option['required']) { ?>
																				<span class="required">*</span>
																				<?php } ?>
																				<b><?php echo $option['name']; ?>:</b><br />
																				<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
																			</div>
																			<br />
																		<?php 
																		} 
																	}
																} 
																?>
														</div>
														<?php 
														if (!$option) {
															if ($quantity_product > 0 ) { 
														?>
														<div class="size form-inline">
															<div class="form-group">
																<label><?php echo $text_qty; ?></label>
																<input type="text" name="quantity" size="2" value="<?php echo $minimum; ?>" class="form-control" />
																<input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
																
																<?php if ($minimum > 1) { ?>
																<div class="minimum"><?php echo $text_minimum; ?></div>
																<?php } ?>
															</div>
															<div class="form-group">
																<input type="button" value="Buy" id="button-cart" class="button btn btn-danger" />
															</div>
														</div>
														<?php 
															} 
														} 
														else {
															if (!$all_option_kosong) {
														?>
															<div class="size form-inline">
																<div class="form-group">
																	<label><?php echo $text_qty; ?></label>
																	<input type="text" name="quantity" size="2" value="<?php echo $minimum; ?>" class="form-control" />
																	<input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
																	
																	<?php if ($minimum > 1) { ?>
																	<div class="minimum"><?php echo $text_minimum; ?></div>
																	<?php } ?>
																</div>
																<div class="form-group">
																	<input type="button" value="Buy" id="button-cart" class="button btn btn-danger" />
																</div>
															</div>
														<?php } 
														}
														?>
													</div>
												</div>
												<?php } ?>
											</div>
											<div class="col-md-12">
												<div id="tab-description" class="tab-content">
													<?php echo $description; ?>
												</div>
												<br/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<?php echo $content_bottom; ?>
			</div>
		</div>
	</div>
</content>
<div class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="mySmallModalLabel">Product option</h4>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});
//--></script> 
<script type="text/javascript"><!--

$('select[name="profile_id"], input[name="quantity"]').change(function(){
		$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name="product_id"], input[name="quantity"], select[name="profile_id"]'),
		dataType: 'json',
				beforeSend: function() {
						$('#profile-description').html('');
				},
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
						
			if (json['success']) {
								$('#profile-description').html(json['success']);
			}	
		}
	});
});
		
$('#button-cart').bind('click', function() {
	// option yg lama
	/*if ($('#optData:has(div#optProduct)').length!=0) {
		if ($('#option-input').val()=="" || $('#option-input').val()=="undefined" || $('#option-item:has(div#option-pr)').length==0) {
			$('.modal-body').empty();
			$('.modal-body').append('Size required');
			$('.bs-modal-sm').modal('show');
		}
		else {
			$.ajax({
				url: 'index.php?route=checkout/cart/add',
				type: 'post',
				data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
				dataType: 'json',
				success: function(json) {
					$('.success, .warning, .attention, information, .error').remove();
					
					if (json['error']) {
						if (json['error']['option']) {
							for (i in json['error']['option']) {
								// $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
								$('.modal-body').append(json['error']['option'][i]);
								$('.bs-modal-sm').modal('show');
							}
						}
										
										if (json['error']['profile']) {
												$('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
										}
					} 
					
					if (json['success']) {
						$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
							
						$('.success').fadeIn('slow');
							
						$('#cart-total').html(json['total']);
						
						// $('html, body').animate({ scrollTop: 0 }, 'slow'); 
					}	
				}
			});
		}
	}
	else {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
			dataType: 'json',
			success: function(json) {
				$('.success, .warning, .attention, information, .error').remove();
				
				if (json['error']) {
					if (json['error']['option']) {
						for (i in json['error']['option']) {
							// $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
							$('.modal-body').append(json['error']['option'][i]);
							$('.bs-modal-sm').modal('show');
						}
					}
									
									if (json['error']['profile']) {
											$('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
									}
				} 
				
				if (json['success']) {
					$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
						
					$('.success').fadeIn('slow');
						
					$('#cart-total').html(json['total']);
					
					// $('html, body').animate({ scrollTop: 0 }, 'slow'); 
				}	
			}
		});
	}*/
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
						// $('.modal-body').append(json['error']['option'][i]);
						// $('.bs-modal-sm').modal('show');
					}
				}
								
				if (json['error']['profile']) {
						$('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
				}
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				// $('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
});
//--></script>

<?php if ($options != false) { ?>
<script type="text/javascript">
function option_data(product_id, option) {	
	$.ajax({
		url: 'index.php?route=product/product/getDetailOption',
		type: 'post',
		data: 'product_id=' +product_id+'&option='+option,
		dataType: 'json',
		success: function(json) {
			$('#option-item').empty();
			if ( json.length != 0 ) {
				$opt='';
				if (json[0]['type']=="select") {
					$opt+='<div id="option-pr" class="option">';
					if (json[0]['required']==1) {
						$opt+='<span class="required">*</span>';
					}
					$opt+='<label>'+json[0]['name']+': </label>';
					$opt+='<select name="option['+json[0]['product_option_id']+']" class="form-control" id="option-input" onchange="option_data_quantity(<?php echo $product_id; ?>,this.value)">';
					$opt+='<option value=""> --- Please Select --- </option>';
					for(var k in json[0]['option_value']) {
						$opt+='<option value="'+json[0]['option_value'][k]['product_option_value_id']+'">'+json[0]['option_value'][k]['name'];
						// if (json[0]['option_value'][k]['price']) {
						// 	$opt+='('+json[0]['option_value'][k]['price_prefix']+' '+json[0]['option_value'][k]['price']+')';
						// }
					$opt+='</option>';
					}
					$opt+='</select>';
					$opt+='</div>';
					if (json[0]['quantity_product']==0) {
						$('#button-cart').attr('value', 'Quote');
					} else {
						$('#button-cart').attr('value', 'Buy');
					}			
				} else if (json[0]['type']=="text") {
					$opt+='<div id="option-pr" class="option">';
					if (json[0]['required']==1) {
						$opt+='<span class="required">*</span>';
					}
					$opt+='<label>'+json[0]['name']+': </label>';
					$opt+='<input type="text" name="option['+json[0]['product_option_id']+']" value="'+json[0]['option_value']+'" class="form-control" id="option-input" placeholder="length x width (in cm)"/>';
					$opt+='</div>';
					$('#button-cart').attr('value', 'Quote');
				}
				$('#option-item').append($opt);
			}
		}
	});
}

function option_data_quantity(product_id, option_value_id) {	
	$.ajax({
		url: 'index.php?route=product/product/getEachDetailOption',
		type: 'post',
		data: 'product_id=' +product_id+'&option_value_id='+option_value_id,
		dataType: 'json',
		success: function(json) {
			if (json['quantity']<=0) {
				$('#button-cart').attr('value', 'Quote');
			} else {
				$('#button-cart').attr('value', 'Buy');
			}
		}
	});
}
</script>
<?php } ?>
<?php echo $footer; ?>