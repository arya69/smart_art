<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content">
					<!-- <div class="focus clearfix"> -->
					<div class="clearfix">
						<div class="content-top-det clearfix">
							<div class="col-md-12">
								<div class="row">
									<div class="focus-new focus-normalize">
										<ul class="nav nav-tabs ul-tab-art">
											<?php if (!empty($_GET['view']) and $_GET['view']=="catalogue") { ?>
												<li class="li-tab-art"><a class="a-tab-art" href="#OVERVIEW" data-toggle="tab">OVERVIEW</a></li>
												<li class="li-tab-art active"><a class="a-tab-art" href="#CATALOGUE" data-toggle="tab">CATALOGUE</a></li>
											<?php } else { ?>
												<li class="li-tab-art active"><a class="a-tab-art" href="#OVERVIEW" data-toggle="tab">OVERVIEW</a></li>
												<li class="li-tab-art"><a class="a-tab-art" href="#CATALOGUE" data-toggle="tab">CATALOGUE</a></li>
											<?php } ?>
											<?php /*
											<li class="cari pull-right">
												<div class="form-inline">
													<div class="form-group">
														<input class="form-control cari" type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
														<button type="submit" class="btn btn-default btn-search-art">
															<i class="fa fa-search"></i>
														</button>
													</div>
												</div>
											</li> */
											?>
										</ul>
									</div>
									
									<div class="focus tab-content clearfix">
										<div class="tab-pane clearfix <?php if (!empty($_GET['view']) and $_GET['view']=="catalogue") {} else { echo "active";} ?>" id="OVERVIEW">											
											<div class="col-md-12">
												<h2 class="text-cinnabar-red"><?php echo $heading_parent_title; ?></h2>
												<hr>
												<?php if ($description_parent) { ?>
											    	<?php echo $description_parent; ?>
											    <?php } ?>
											</div>
										</div>
										<div class="tab-pane clearfix <?php if (!empty($_GET['view']) and $_GET['view']=="catalogue") { echo "active";} ?>" id="CATALOGUE">
											<div class="pull-right limit-pr-data">
												<!-- <div class="focus-top clearfix limit"> -->
													<!-- <div class="col-md-12 text-right"> -->
														<div class="size form-inline size-onpage">
															<div class="form-group">
																<label><b><?php echo $text_limit; ?></b></label>
																<select onchange="location = this.value;" class="form-control">
																	<?php foreach ($limits as $limits) { ?>
																		<?php if ($limits['value'] == $limit) { ?>
																			<option value="<?php echo $limits['href']; ?>" selected="selected">
																				<?php echo $limits['text']; ?>
																			</option>
																		<?php } else { ?>
																			<option value="<?php echo $limits['href']; ?>">
																				<?php echo $limits['text']; ?>
																			</option>
																		<?php } ?>
																	<?php } ?>
																</select>
															</div>
														</div>
													<!-- </div> -->
												<!-- </div> -->
											</div>
											<?php echo $content_top; ?>
											<div class="col-md-12">
												<div class="non-row">
													<div class="col-md-12">
													<?php if ($category_id!=$category_parent_id) { ?>
														<div class="focus-top clearfix limit">
															<div class="col-md-12">
																<h3 class="text-cinnabar-red"><?php echo $heading_title; ?></h3>
																<?php if ($description) { ?>
															    	<?php echo $description; ?>
															    <?php } ?>
															</div>
														</div>											
													<?php } ?>
													<hr>
													</div>
												</div>
											</div>
											
											<div class="col-md-12">
											<?php /*var_dump($products);*/ foreach ($products as $product) { ?>
											<div class="loop-gall col-md-4 text-center ngg-gallery-thumbnail">
												<a href="<?php echo $product['href']; ?>" class="link-det-img">
													<?php if (!empty($product['square_thumb'])) { ?>
														<img class="img-det" src="<?php echo $product['square_thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
													<?php } else { ?>
														<img class="img-det" src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
													<?php } ?>
													<br/>
													<h4 class="product-name"><?php echo $product['name']; ?></h4>
												</a>
											</div>
											<?php } ?>
											</div>
											
											<?php /*
											<div class="col-md-4 clearfix text-center">
												<div class="row">
													<div class="focus-top clearfix inf-img">
														<div class="col-md-12 pros">
															<div class="box-product">
																<div class="col-md-3">
																	<?php if ($product['thumb']) { ?>
																		<div class="product-detail-img">
																			<div class="loop-gall col-md-4 text-center">
																				<div class="image">
																					<a href="<?php echo $product['href']; ?>" class="link-det-img">
																						<img class="img-det" src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
																					</a>
																				</div>
																			</div>
																		 </div>
																	<?php } ?>
																</div>
															</div> 
														</div>
													</div>
												</div>
											</div>
											*/ ?>
											
											<div class="row">
												<div class="col-md-12">
													<!-- <div class="pagination"><?php echo $pagination; ?></div> -->
													<div class="gall-pagination col-md-11 col-xs-11 col-xs-offset-1 text-right pagination">
														<ul class="pagination">
															<!-- <li> -->
																<?php echo $pagination; ?>
															<!-- </li> -->
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php echo $content_bottom; ?>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?>