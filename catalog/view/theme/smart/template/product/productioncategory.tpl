<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<content>
	<div id="container" class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 clearfix">
				<div id="content">
					<!-- <div class="focus clearfix"> -->
					<div class="clearfix">
						<div class="content-top-det clearfix">
							<div class="col-md-12">
								<div class="row">
									<div class="focus tab-content clearfix">
										<div class="tab-pane clearfix active" id="CATALOGUE">
											<?php //echo $content_top; ?>
											
											<div class="col-md-12">
												<h2 class="text-cinnabar-red lineup-heading">Product Category</h2>
												<?php foreach ($productions as $production) { ?>
												<div class="loop-gall col-md-4 text-center ngg-gallery-thumbnail">
													<a href="<?php echo $production['href']; ?>" class="link-det-img">
													<img class="img-det" src="<?php echo $production['image']; ?>" title="<?php echo $production['name']; ?>" alt="<?php echo $production['name']; ?>" /><br/>
													<h4 class="product-name"><?php echo $production['name']; ?></h4>
													</a>
												</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php echo $content_bottom; ?>
				</div>
			</div>
		</div>
	</div>
</content>
<?php echo $footer; ?>