<?php 
class ModelPaymentEmailCheckout extends Model {
  	public function getMethod($address, $total) {
		$this->language->load('payment/email_checkout');
		
		if ($total >= 0) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();
			
		if ($status) {  
			$method_data = array( 
				'code'       => 'email_checkout',
				'title'      => $this->language->get('text_title'),
				'decription'      => $this->language->get('text_decription'),
				'sort_order' => $this->config->get('email_checkout_sort_order')
			);
		}
		
    	return $method_data;
  	}
}
?>