<?php
class ModelShippingInfobyemail extends Model {
	function getQuote($address) {
		$this->language->load('shipping/infobyemail');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('infobyemail_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('infobyemail_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		if ($this->cart->getSubTotal() < $this->config->get('infobyemail_total')) {
			$status = false;
		}
		
		$method_data = array();
	
		if ($status) {
			$quote_data = array();
			
      		$quote_data['infobyemail'] = array(
        		'code'         => 'infobyemail.infobyemail',
        		'title'        => $this->language->get('text_description'),
        		'cost'         => 'We will contact you by email for details',
        		'tax_class_id' => 'We will contact you by email for details',
				'text'         => ''
      		);

      		$method_data = array(
        		'code'       => 'infobyemail',
        		'title'      => $this->language->get('text_title'),
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('infobyemail_sort_order'),
        		'error'      => false
      		);
		}
		// print_r($method_data);
		// die();
	
		return $method_data;
	}
}
?>