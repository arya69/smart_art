<?php
class ModelShippingCouriersplease extends Model {
	function getQuote($address) {
		$this->language->load('shipping/couriersplease');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('couriersplease_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('couriersplease_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		if ($this->customer->isLogged() && isset($this->session->data['shipping_address_id'])) {					
			$shipping_address = $this->model_account_address->getAddress($this->session->data['shipping_address_id']);		
		} elseif (isset($this->session->data['guest'])) {
			$shipping_address = $this->session->data['guest']['shipping'];
		}

		$error = '';

		$quote_data = array();

		$metroPrice=0;
		$ezyPrice=0;
		$cp=0;

		if ($status) {
			$weight = $this->cart->getWeight();
			$cartProduct=$this->cart->getProducts();

			foreach ($cartProduct as $key => $cartItem) {
				$length=$cartItem['length'];
				$height=$cartItem['height'];
				$width=$cartItem['width'];
				$weight=$cartItem['weight_item'];
				$destpostcode=$shipping_address["postcode"];
				$originpostcode='2017';
				$originsuburb='Zetland';
				$destsuburb=$shipping_address["suburb"];

				$courierspleaseUrl = 'http://www.couriersplease.com.au/cpnCalcXml.php?length='.$length.'&height='.$height.'&width='.$width.'&weight='.$weight.'&destpostcode='.$destpostcode.'&originpostcode='.$originpostcode.'&originsuburb='.$originsuburb.'&destsuburb='.$destsuburb;		

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch,CURLOPT_URL, $courierspleaseUrl);

				$response = curl_exec($ch);
				curl_close($ch);

				$dom = new DOMDocument('1.0', 'UTF-8');
				$dom->loadXml($response);

				$errorDom=$dom->getElementsByTagName('error')->length;

				if ($errorDom!=0) {
					$error = $dom->getElementsByTagName('error')->item(0)->getAttribute('message');
					break;
				} else {				
					$originpostcode=$dom->getElementsByTagName('originpostcode');
					foreach ($originpostcode as $key => $data1) {
						$originpostcodeValue=$data1->getAttribute('value');
					}
					
					$originsuburb=$dom->getElementsByTagName('originsuburb');
					foreach ($originsuburb as $key => $data2) {
						$originsuburbValue=$data2->getAttribute('value');
					}
					
					$destpostcode=$dom->getElementsByTagName('destpostcode');
					foreach ($destpostcode as $key => $data3) {
						$destpostcodeValue=$data3->getAttribute('value');
					}
					
					$destsuburb=$dom->getElementsByTagName('destsuburb');
					foreach ($destsuburb as $key => $data4) {
						$destsuburbValue=$data4->getAttribute('value');
					}
					
					$destsuburb=$dom->getElementsByTagName('destsuburb');
					foreach ($destsuburb as $key => $data5) {
						$destsuburbValue=$data5->getAttribute('value');
					}

					$metroCoupons=$dom->getElementsByTagName('metroCoupons');
					foreach ($metroCoupons as $key => $data6) {
						$metroCouponsValue=$data6->getAttribute('qty');
					}

					$linkCoupons=$dom->getElementsByTagName('linkCoupons');
					foreach ($linkCoupons as $key => $data7) {
						$linkCouponsValue=$data7->getAttribute('qty');
					}

					$deliveryDays=$dom->getElementsByTagName('deliveryDays');
					foreach ($deliveryDays as $key => $data8) {
						$deliveryDaysValue=$data8->getAttribute('value');
					}

					$metro=$metroCouponsValue*$this->config->get('config_metro_cp_price')*$cartItem['quantity'];
					$ezy=$linkCouponsValue*$this->config->get('config_ezy_link_cp_price')*$cartItem['quantity'];

					$totalMetroPrice+=$metro;
					$totalEzyPrice+=$ezy;					

					// var_dump($metroCouponsValue);
					// var_dump($linkCouponsValue);
					// $mtr=$metroCouponsValue*$this->config->get('config_metro_cp_price');
					// $ez=$linkCouponsValue*$this->config->get('config_ezy_link_cp_price');
					// echo 'width-->'.$cartItem['width'].'<br/>length-->'.$cartItem['length'].'<br/>height-->'.$cartItem['height'].'<br/>weight-->'.$cartItem['weight'].'<br/>weight_item-->'.$cartItem['weight_item'].'<br/>cp-->'.$cp;
					// echo '<br/>----------------------------------------------------------------------------<br/>';
					// echo 'mtr-->'.$mtr.'<br/>ez-->'.$ez.'<br/>metro-->'.$metro.'<br/>ezy-->'.$ezy.'<br/>totalMetroPrice-->'.$totalMetroPrice.'<br/>totalEzyPrice-->'.$totalEzyPrice.'<br/>cp-->'.$cp;
					// echo '<br/><br/><br/>';

					// $cp++;
				}
			}
			// die();

			$cost=$totalMetroPrice+$totalEzyPrice;
			$title = $this->language->get('text_title');
			// $estimate="(Delivery days: ".$deliveryDaysValue.")";
			// var_dump($deliveryDaysValue);

			$quote_data['couriersplease'] = array(
				'code'         => 'couriersplease.couriersplease.smartart',
				'title'        => $title,
				'cost'         => $cost,
				'deliveryDay'  => $deliveryDaysValue,
				'tax_class_id' => $this->config->get('couriersplease_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($cost)),
			);
		}

		$method_data = array();

		if ($quote_data || $error) {
			$title = $this->language->get('text_title');

			$method_data = array(
				'code'       => 'couriersplease',
				'title'      => $title,
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('couriersplease_sort_order'),
				'error'      => $error
			);
		}

		return $method_data;
	}
}		
?>