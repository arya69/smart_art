<?php 
class ControllerProductProductioncategory extends Controller {  
	public function index() { 
		$this->language->load('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$category_info = $this->model_catalog_category->getProductionCategories();

		if ($category_info) {
			$this->document->setTitle('PRODUCTION');
			$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');

			$this->data['heading_title'] = 'Product Category';
			$this->data['production'] = array();

			foreach ($category_info as $key => $info) {
				if ($info['category_id']!=61) {
					if ($info['image']) {
						$thumb = $this->model_tool_image->resize($info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
					} else {
						$thumb = '';
					}

					$description = html_entity_decode($info['description'], ENT_QUOTES, 'UTF-8');		
					$name = html_entity_decode($info['name'], ENT_QUOTES, 'UTF-8');
					
					$this->data['productions'][]=array(
						'image'			=>$thumb,
						'description'	=>$description,
						'name'			=>$name,
						'href'  		=>$this->url->link('product/category', 'path='.$info['category_id']),
					);
				}
			}

			// var_dump($this->data['productions']);

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/productioncategory.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/productioncategory.tpl';
			} else {
				$this->template = 'default/template/product/productioncategory.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());										
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/category', $url),
				'separator' => $this->language->get('text_separator')
			);

			$this->document->setTitle($this->language->get('text_error'));

			$this->data['heading_title'] = $this->language->get('text_error');

			$this->data['text_error'] = $this->language->get('text_error');

			$this->data['button_continue'] = $this->language->get('button_continue');

			$this->data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . '/1.1 404 Not Found');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

			$this->response->setOutput($this->render());
		}
	}
}
?>