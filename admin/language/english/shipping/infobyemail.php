<?php
// Heading
$_['heading_title']    = 'Info by email Shipping';

// Text 
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Info by email shipping!';

// Entry
$_['entry_total']      = 'Total:<br /><span class="help">Sub-Total amount needed before the Info by email shipping module becomes available.</span>';
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Info by email shipping!';
?>