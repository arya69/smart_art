<?php
      //===========================================//
     // Customer Group Checkout Requirements      //
    // Author: Joel Reeds                        //
   // Company: OpenCart Addons                  //
  // Website: http://opencartaddons.com        //
 // Contact: webmaster@opencartaddons.com     //
//===========================================//

// Heading
$_['heading_title']    		= 'Customer Group Checkout Requirements | <a href="http://opencartaddons.com" target="_blank">OpenCartAddons</a> |';
$_['text_name']    			= 'Customer Group Checkout Requirements';
$_['text_module']    		= 'Module';

$_['text_min']				= 'Min';
$_['text_max']				= 'Max';

$_['text_guest_checkout']	= 'Guest Checkout';

$_['text_contact']      	= '<span class="help">Copyright &copy;' . date("Y") . ' OpenCart Addons - v%s - Visit <a href="http://opencartaddons.com" target="_blank">http://opencartaddons.com</a> For Support.</span>';

$_['text_settings']			= 'Settings';
$_['text_help']				= 'Help Section';

//Buttons
$_['button_apply']			= 'Apply';

//General Settings
$_['entry_system_status']	= '<span class="required">*</span> System Status<br/><span class="help">Overall System Status</span>';
$_['entry_status']			= '<span class="required">*</span> Status<br/><span class="help">Customer Group Status</span>';
$_['entry_customer_group']	= 'Customer Group';
$_['entry_total']			= 'Sub-Total<br/><span class="help">Cart Sub-Total</span>';
$_['entry_quantity']		= 'Quantity<br/><span class="help">Total Cart Quantity</span>';
$_['entry_weight']			= 'Weight(%s)<br/><span class="help">Total Cart Weight</span>';
$_['entry_product'] 		= 'Product Quantity<br/><span class="help">Individual Product Quantity</span>';

// Error
$_['text_success']    		= 'Success: You have modified Customer Group Checkout Requirements!';
$_['error_permission'] 	 	= 'Warning: You do not have permission to modify Customer Group Checkout Requirements!';
?>