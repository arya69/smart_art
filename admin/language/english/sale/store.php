<?php
// Heading
$_['heading_title']                           	= 'Store';

//Text
$_['text_edit']                               	= 'Edit';
$_['text_store']                          		= 'Store:';
$_['text_success']                            	= 'Success: You have modified outlet request!';
$_['text_address']                      		= 'Address:';
$_['text_city_provinces']                       = 'City & Provinces:';
$_['text_phone']                               	= 'Phone:';
$_['text_owner']                          		= 'Owner:';
$_['text_opening']                          	= 'Opening:';
$_['text_error']                              	= 'Error:<br /><span class="help">Returns an error string with a warning message or a reason why the request failed.</span>';

//ENTRY
$_['entry_store']      		                   	= 'Store:';
$_['entry_city_provinces']                      = 'City & Provinces:';
$_['entry_owner']                              	= 'Owner:';
$_['entry_phone']	                         	= 'Phone:';
$_['entry_address']	                         	= 'Address:';
$_['entry_opening']	                         	= 'Opening:';

// Column
$_['column_id_store']                       	= 'Id Store';
$_['column_store']                       		= 'Store';
$_['column_city_provinces']                     = 'City & Provinces';
$_['column_owner']                        		= 'Owner';
$_['column_phone']                        	  	= 'Phone';
$_['column_address']                       	 	= 'Address';
$_['column_opening']                       	 	= 'Opening';
$_['column_action']                          	= 'Action';

// Error
$_['error_exists'] 							  = 'Warning: E-Mail Address is already registered!';
$_['error_warning']                           = 'Warning: Please check the form carefully for errors!';
$_['error_permission']                        = 'Warning: You do not have permission to modify orders!';
$_['error_full_name']                         = 'Full Name must be between 10 and 32 characters!';
$_['error_city_provinces']                    = 'Warning: City & Provinces required!';
$_['error_city']                              = 'City must be between 3 and 128 characters!';
$_['error_post_code']                         = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_action']                            = 'Warning: Could not complete this action!';
$_['error_store']                             = 'Warning: Store name required!';
$_['error_phone']                             = 'Warning: Phone number required!';
$_['error_address']                           = 'Warning: Address required!';
$_['error_opening']                           = 'Warning: Opening required!';
?>