<?php echo $header; ?>
<style>
	.hidden
	{
		display: none !important;
	}
	
	.width-auto
	{
		width: auto;
	}
</style>

<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
	<p>
		Thank you for your purchase, please check <a href="http://www.opencart.com/index.php?route=extension/extension&filter_username=WeDoWeb" title="WeDoWeb's OpenCart extensions">here</a> for more useful extensions.<br/>
		
		<b>WeDoWeb Team.</b>
	</p>
  
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table id="module" class="form">
        <tbody>
          <tr>
            <td>
				<?php echo $entry_display_multiple_prices; ?>
				<br><span class="help"></span>
			</td>
            <td>
				<input type="radio" name="cgp_display_multiple_prices" value="1" <?php echo $cgp_display_multiple_prices ? 'checked="checked"' : ''; ?> ><?php echo $text_yes;?>
				<input type="radio" name="cgp_display_multiple_prices" value="0" <?php echo !$cgp_display_multiple_prices ? 'checked="checked"' : ''; ?> ><?php echo $text_no;?>
			</td>
          </tr>
        </tbody>
      </table>
	  <table class="list width-auto" id="tbl_customer_group_settings">
		<thead>
			<tr>
				<td class="left"><?php echo $entry_customer_group_name; ?>
				<br><span class="help"></span>
				</td>
				<td class="left"><?php echo $entry_show_as_reference_on_product_page; ?>
				<br><span class="help"></span>
				</td>
				<td class="right"><?php echo $entry_sort_order; ?>
				<br><span class="help"></span>
				</td>
				<td class="left hidden"><?php echo $entry_use_custom_style; ?>
				<br><span class="help"></span>
				</td>
				<td class="left hidden"><?php echo $entry_text_color; ?>
				<br><span class="help"></span>
				</td>
				<td class="left hidden"><?php echo $entry_apply_style_to; ?>
				<br><span class="help"></span>
				</td>
			</tr>
		</thead>
		<tbody>
			<?php
				$row = -1;
				foreach($cgp_customer_group_settings as $customer_group_settings) {
				$row++;
			?>
				<tr>
					<td class="left">
						<?php echo $customer_group_settings['name']; ?>
						<input type="hidden" name="cgp_customer_group_settings[<?php echo $row; ?>][customer_group_id]" value="<?php echo $customer_group_settings['customer_group_id']; ?>"/>
					</td>
					<td class="left">
						<input type="checkbox" name="cgp_customer_group_settings[<?php echo $row; ?>][show_as_reference_on_product_page]" value="1" <?php echo $customer_group_settings['show_as_reference_on_product_page'] ? 'checked="checked"' : ''; ?> >
					</td>
					<td class="right">
						<input type="text" name="cgp_customer_group_settings[<?php echo $row; ?>][sort_order]" value="<?php echo $customer_group_settings['sort_order']; ?>" size="1" />
					</td>
					<td class="left hidden">
						<input type="checkbox" name="cgp_customer_group_settings[<?php echo $row; ?>][use_custom_style]" value="1" <?php echo $customer_group_settings['use_custom_style'] ? 'checked="checked"' : ''; ?> >
					</td>
					<td class="left hidden">
						<input type="text" name="cgp_customer_group_settings[<?php echo $row; ?>][text_color]" value="<?php echo $customer_group_settings['text_color']; ?>" readonly="true" id="color-input<?php echo $row; ?>" class="color-input" />
					</td>
					<td class="left hidden">
						<input type="checkbox" name="cgp_customer_group_settings[<?php echo $row; ?>][apply_style_to_name]" value="1" <?php echo $customer_group_settings['apply_style_to_name'] ? 'checked="checked"' : ''; ?> ><?php echo $text_name; ?>
						<input type="checkbox" name="cgp_customer_group_settings[<?php echo $row; ?>][apply_style_to_text]" value="1" <?php echo $customer_group_settings['apply_style_to_text'] ? 'checked="checked"' : ''; ?> ><?php echo $text_price; ?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	  </table>
    </form>
  </div>
</div>
<link rel="stylesheet" href="view/stylesheet/colorpicker/colorpicker.css" type="text/css" />
<script type="text/javascript" src="view/javascript/colorpicker/colorpicker.js"></script>
<script type="text/javascript" src="view/javascript/colorpicker/eye.js"></script>
<script type="text/javascript" src="view/javascript/colorpicker/utils.js"></script>
<script type="text/javascript" src="view/javascript/colorpicker/layout.js?ver=1.0.2"></script>
<script type="text/javascript" src="view/javascript/colorpicker/misc.js"></script>
<script type="text/javascript">
	$(document).ready(
		function()
		{
			colorPickerise($('.color-input'));
		}
	);
</script>
<?php echo $footer; ?>