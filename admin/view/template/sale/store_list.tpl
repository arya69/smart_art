<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
	  <div class="buttons">
		  <a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a>
		  <a onclick="$('#form').attr('action', '<?php echo $delete; ?>'); $('#form').attr('target', '_self'); $('#form').submit();" class="button"><?php echo $button_delete; ?></a>
	  </div>
    </div>
    <div class="content">
      <form action="" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
			  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php echo $column_id_store; ?></td>
              <td class="right"><?php echo $column_city_provinces; ?></td>
              <td class="right"><?php echo $column_store; ?></td>
			  <td class="right"><?php echo $column_phone; ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
			<tr class="filter">
			  <td></td>
              <td align="right"><input type="text" name="filter_id_store" value="<?php echo $filter_id_store; ?>" size="4" style="text-align: right;" /></td>
              <td align="right"><input type="text" name="filter_city_provinces" value="<?php echo $filter_city_provinces; ?>" style="text-align: right;" /></td>
              <td align="right"><input type="text" name="filter_store" value="<?php echo $filter_store; ?>" style="text-align: right;" /></td>
              <td align="right"><input type="text" name="filter_phone" value="<?php echo $filter_phone; ?>" style="text-align: right;" /></td>
              <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>
            <?php if ($store) { ?>
            <?php foreach ($store as $outlet) { ?>
            <tr>
			  <td style="text-align: center;"><?php if ($outlet['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $outlet['id_store']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $outlet['id_store']; ?>" />
                <?php } ?></td>
              <td class="right"><?php echo $outlet['id_store']; ?></td>
              <td class="right"><?php echo $outlet['city_provinces']; ?></td>
              <td class="right"><?php echo $outlet['store']; ?></td>
              <td class="right"><?php echo $outlet['phone']; ?></td>
              <td class="right"><?php foreach ($outlet['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=sale/store&token=<?php echo $token; ?>';
	
	var filter_id_store = $('input[name=\'filter_id_store\']').attr('value');	
	if (filter_id_store) {
		url += '&filter_id_store=' + encodeURIComponent(filter_id_store);
	}
	
	var filter_city_provinces = $('input[name=\'filter_city_provinces\']').attr('value');	
	if (filter_city_provinces) {
		url += '&filter_city_provinces=' + encodeURIComponent(filter_city_provinces);
	}
	
	var filter_store = $('input[name=\'filter_store\']').attr('value');
	if (filter_store) {
		url += '&filter_store=' + encodeURIComponent(filter_store);
	}	
	
	var filter_phone = $('input[name=\'filter_phone\']').attr('value');	
	if (filter_phone) {
		url += '&filter_phone=' + encodeURIComponent(filter_phone);
	}

	location = url;
}
//--></script>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script>
<?php echo $footer; ?>