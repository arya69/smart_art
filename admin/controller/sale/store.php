<?php
class ControllerSaleStore extends Controller {
	private $error = array();

  	public function index() {
		$this->language->load('sale/store');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/store');

    	$this->getList();
  	}
	
	protected function getList() {
		if (isset($this->request->get['filter_id_store'])) {
			$filter_id_store = $this->request->get['filter_id_store'];
		} else {
			$filter_id_store = null;
		}

		if (isset($this->request->get['filter_city_provinces'])) {
			$filter_city_provinces = $this->request->get['filter_city_provinces'];
		} else {
			$filter_city_provinces = null;
		}

		if (isset($this->request->get['filter_store'])) {
			$filter_store = $this->request->get['filter_store'];
		} else {
			$filter_store = null;
		}
		
		if (isset($this->request->get['filter_phone'])) {
			$filter_phone = $this->request->get['filter_phone'];
		} else {
			$filter_phone = null;
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id_store';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
				
		$url = '';
		
		if (isset($this->request->get['filter_id_store'])) {
			$url .= '&filter_id_store=' . $this->request->get['filter_id_store'];
		}
		
		if (isset($this->request->get['filter_city_provinces'])) {
			$url .= '&filter_city_provinces=' . urlencode(html_entity_decode($this->request->get['filter_city_provinces'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_store'])) {
			$url .= '&filter_store=' . urlencode(html_entity_decode($this->request->get['filter_store'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_phone'])) {
			$url .= '&filter_phone=' . urlencode(html_entity_decode($this->request->get['filter_phone'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);

		$this->data['insert'] = $this->url->link('sale/store/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['delete'] = $this->url->link('sale/store/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['store'] = array();

		$data = array(
			'filter_id_store'=>$filter_id_store,
			'filter_city_provinces'=>$filter_city_provinces,
			'filter_store'=>$filter_store,
			'filter_phone'=>$filter_phone,
			'start'=>($page - 1) * $this->config->get('config_admin_limit'),
			'limit'=>$this->config->get('config_admin_limit')
		);

		$Store_total = $this->model_sale_store->getTotalStore($data);

		$results = $this->model_sale_store->getStore($data);

    	foreach ($results as $result) {
			$action = array();
						
			$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('sale/store/update', 'token=' . $this->session->data['token'] . '&id_store=' . $result['id_store'] . $url, 'SSL')
				);
		
			$this->data['store'][] = array(
				'id_store'  	=> $result['id_store'],
				'city_provinces' => $result['city_provinces'],
				'store'   		=> $result['store'],
				'phone'   		=> $result['phone'],
				'selected'    	=> isset($this->request->post['selected']) && in_array($result['id_store'], $this->request->post['selected']),
				'action'      	=> $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_missing'] = $this->language->get('text_missing');

		$this->data['column_id_store'] = $this->language->get('column_id_store');
    	$this->data['column_city_provinces'] = $this->language->get('column_city_provinces');
		$this->data['column_store'] = $this->language->get('column_store');
		$this->data['column_phone'] = $this->language->get('column_phone');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';
		
		if (isset($this->request->get['filter_id_store'])) {
			$url .= '&filter_id_store=' . $this->request->get['filter_id_store'];
		}
		
		if (isset($this->request->get['filter_city_provinces'])) {
			$url .= '&filter_city_provinces=' . urlencode(html_entity_decode($this->request->get['filter_city_provinces'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_store'])) {
			$url .= '&filter_store=' . urlencode(html_entity_decode($this->request->get['filter_store'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_phone'])) {
			$url .= '&filter_phone=' . urlencode(html_entity_decode($this->request->get['filter_phone'], ENT_QUOTES, 'UTF-8'));
		}
		
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_id_store'] = $this->url->link('sale/store', 'token=' . $this->session->data['token'] . '&sort=id_store' . $url, 'SSL');
		$this->data['sort_city_provinces'] = $this->url->link('sale/store', 'token=' . $this->session->data['token'] . '&sort=city_provinces' . $url, 'SSL');
		$this->data['sort_store'] = $this->url->link('sale/store', 'token=' . $this->session->data['token'] . '&sort=store' . $url, 'SSL');
		$this->data['sort_phone'] = $this->url->link('sale/store', 'token=' . $this->session->data['token'] . '&sort=phone' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['filter_id_store'])) {
			$url .= '&filter_id_store=' . $this->request->get['filter_id_store'];
		}
		
		if (isset($this->request->get['filter_city_provinces'])) {
			$url .= '&filter_city_provinces=' . urlencode(html_entity_decode($this->request->get['filter_city_provinces'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_store'])) {
			$url .= '&filter_store=' . urlencode(html_entity_decode($this->request->get['filter_store'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_phone'])) {
			$url .= '&filter_phone=' . urlencode(html_entity_decode($this->request->get['filter_phone'], ENT_QUOTES, 'UTF-8'));
		}

		$pagination = new Pagination();
		$pagination->total = $Store_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('sale/store', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['filter_id_store'] = $filter_id_store;
		$this->data['filter_city_provinces'] = $filter_city_provinces;
		$this->data['filter_store'] = $filter_store;
		$this->data['filter_phone'] = $filter_phone;
		
		$this->template = 'sale/store_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
  	}
	
	public function insert() {
		$this->language->load('sale/store');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/store');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
      	  	$this->model_sale_store->addStore($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
		  
			$url = '';
			
			if (isset($this->request->get['filter_id_store'])) {
				$url .= '&filter_id_store=' . $this->request->get['filter_id_store'];
			}
			
			if (isset($this->request->get['filter_city_provinces'])) {
				$url .= '&filter_city_provinces=' . urlencode(html_entity_decode($this->request->get['filter_city_provinces'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_store'])) {
				$url .= '&filter_store=' . urlencode(html_entity_decode($this->request->get['filter_store'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_phone'])) {
				$url .= '&filter_phone=' . urlencode(html_entity_decode($this->request->get['filter_phone'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('sale/store', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		
    	$this->getForm();
  	}
	
	public function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');  
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_missing'] = $this->language->get('text_missing');

		$this->data['column_id_store'] = $this->language->get('column_id_store');
    	$this->data['column_city_provinces'] = $this->language->get('column_city_provinces');
		$this->data['column_store'] = $this->language->get('column_store');
		$this->data['column_phone'] = $this->language->get('column_phone');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
		$this->data['entry_city_provinces'] = $this->language->get('entry_city_provinces');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_phone'] = $this->language->get('entry_phone');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
 		if (isset($this->error['city_provinces'])) {
			$this->data['error_city_provinces'] = $this->error['city_provinces'];
		} else {
			$this->data['error_city_provinces'] = '';
		}
		
		if (isset($this->error['store'])) {
			$this->data['error_store'] = $this->error['store'];
		} else {
			$this->data['error_store'] = '';
		}
		
		if (isset($this->error['phone'])) {
			$this->data['error_phone'] = $this->error['phone'];
		} else {
			$this->data['error_phone'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_id_store'];
		}
		
		if (isset($this->request->get['filter_city_provinces'])) {
			$url .= '&filter_city_provinces=' . urlencode(html_entity_decode($this->request->get['filter_city_provinces'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_store'])) {
			$url .= '&filter_store=' . urlencode(html_entity_decode($this->request->get['filter_store'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_phone'])) {
			$url .= '&filter_phone=' . urlencode(html_entity_decode($this->request->get['filter_phone'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('sale/store', 'token=' . $this->session->data['token'] . $url, 'SSL'),				
			'separator' => ' :: '
		);

		if (!isset($this->request->get['id_store'])) {
			$this->data['action'] = $this->url->link('sale/store/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('sale/store/update', 'token=' . $this->session->data['token'] . '&id_store=' . $this->request->get['id_store'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('sale/store', 'token=' . $this->session->data['token'] . $url, 'SSL');

    	if (isset($this->request->get['id_store']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$store_info = $this->model_sale_store->getStoreRequest($this->request->get['id_store']);      		
    	}
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['id_store'])) {
			$this->data['id_store'] = $this->request->get['id_store'];
		} else {
			$this->data['id_store'] = 0;
		}
					
		if (isset($this->request->post['city_provinces'])) {
      		$this->data['city_provinces'] = $this->request->post['city_provinces'];
		} elseif (!empty($store_info)) { 
			$this->data['city_provinces'] = $store_info['city_provinces'];
		} else {
      		$this->data['city_provinces'] = '';
    	}

    	if (isset($this->request->post['store'])) {
      		$this->data['store'] = $this->request->post['store'];
    	} elseif (!empty($store_info)) { 
			$this->data['store'] = $store_info['store'];
		} else {
      		$this->data['store'] = '';
    	}

    	if (isset($this->request->post['phone'])) {
      		$this->data['phone'] = $this->request->post['phone'];
    	} elseif (!empty($store_info)) { 
			$this->data['phone'] = $store_info['phone'];
		} else {
      		$this->data['phone'] = '';
    	}

    	if (isset($this->request->get['id_store'])) {
			$outlet = $this->model_sale_store->getStoreRequest($this->request->get['id_store']);			
		} else {
			$outlet = array();
		}
		
		//$this->data['store'] = array();	
		//print_r($outlet);
		//echo $outlets['full_name'];
		/*
		foreach ($outlet as $toko) {							
			$this->data['store'][] = array(
				'id_store'=>$toko['id_store'],
				'city_provinces'=>$toko['city_provinces'],
				'full_name'=>$toko['full_name'],
				'address'=>$toko['address'],
				'phone'=>$toko['phone'],
				'owner'=>$toko['owner'],
			);
		}
		*/
		$this->template = 'sale/store_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
  	}
	
	protected function validateForm() {
    	if (!$this->user->hasPermission('modify', 'sale/store')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}
		
		if (!isset($this->request->get['id_store'])) {
			if ($store) {
				$this->error['warning'] = $this->language->get('error_exists');
			}
		} else {
			if ($store && ($this->request->get['id_store'] != $store['id_store'])) {
				$this->error['warning'] = $this->language->get('error_exists');
			}
		}

    	if (empty($this->request->post['store'])) {
      		$this->error['store'] = $this->language->get('error_store');
    	}
		
		if (empty($this->request->post['city_provinces'])) {
      		$this->error['city_provinces'] = $this->language->get('error_city_provinces');
    	}
		
		if (empty($this->request->post['phone'])) {
      		$this->error['phone'] = $this->language->get('error_phone');
    	}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}
		
	public function delete() {
		$this->language->load('sale/store');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/store');

    	if (isset($this->request->post['selected']) && ($this->validateDelete())) {
			foreach ($this->request->post['selected'] as $order_id) {
				$this->model_sale_store->deleteStore($order_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_id_store'])) {
			$url .= '&filter_id_store=' . $this->request->get['filter_id_store'];
			}
			
			if (isset($this->request->get['filter_city_provinces'])) {
				$url .= '&filter_city_provinces=' . urlencode(html_entity_decode($this->request->get['filter_city_provinces'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_store'])) {
				$url .= '&filter_store=' . urlencode(html_entity_decode($this->request->get['filter_store'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_phone'])) {
				$url .= '&filter_phone=' . urlencode(html_entity_decode($this->request->get['filter_phone'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('sale/store', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    	}

    	$this->getList();
  	}
	
	protected function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'sale/store')) {
			$this->error['warning'] = $this->language->get('error_permission');
    	}

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}
	
	public function update() {
		$this->language->load('sale/store');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/store');
    	
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_store->editStore($this->request->get['id_store'], $this->request->post);
	  		
			$this->session->data['success'] = $this->language->get('text_success');
	  
			$url = '';

			if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_id_store'];
			}
			
			if (isset($this->request->get['filter_city_provinces'])) {
				$url .= '&filter_city_provinces=' . urlencode(html_entity_decode($this->request->get['filter_city_provinces'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_store'])) {
				$url .= '&filter_store=' . urlencode(html_entity_decode($this->request->get['filter_store'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_phone'])) {
				$url .= '&filter_phone=' . urlencode(html_entity_decode($this->request->get['filter_phone'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('sale/store', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		
    	$this->getForm();
  	}
}
?>
