<?php
class ControllerModuleCgp extends Controller {
	private $error = array(); 
	
	public function index() {  
	
		$this->load->language('module/cgp');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('cgp', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/cgp', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/cgp', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['modules'] = array();
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
						
		$this->template = 'module/cgp.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		//START
		$this->load->model('tool/image');
		$this->load->model('module/cgp');
		
		//entry
		$this->setDataLang('entry_display_multiple_prices');
		$this->setDataLang('entry_customer_group_name');
		$this->setDataLang('entry_enable');
		$this->setDataLang('entry_show_as_reference_on_product_page');
		$this->setDataLang('entry_sort_order');
		$this->setDataLang('entry_use_custom_style');
		$this->setDataLang('entry_text_color');
		$this->setDataLang('entry_apply_style_to');
		
		//text
		$this->setDataLang('text_yes');
		$this->setDataLang('text_no');
		$this->setDataLang('text_name');
		$this->setDataLang('text_price');
		
		//data		
		$this->setData('cgp_enable', 'true');
		$this->setData('cgp_display_multiple_prices', 'false');
		$this->data['cgp_customer_group_settings'] = $this->model_module_cgp->getCustomerGroupSettings();
		
		$this->data['token'] =  $this->session->data['token'];
		//END
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/cgp')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	public function install()
	{
		// TODO set cgp_enable true
	}
	
	public function uninstall()
	{
		// TODO set cgp_enable false
	}
}
?>