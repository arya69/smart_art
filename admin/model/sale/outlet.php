<?php
class ModelSaleOutlet extends Model {
	public function getOutlet($data = array()) {
		$sql = "SELECT * FROM `outlet_request`";
		
		if (isset($data['filter_id_request']) && !is_null($data['filter_id_request'])) {
			$sql .= " WHERE id_request = '" . (int)$data['filter_id_request'] . "'";
		} else {
			$sql .= " WHERE id_request > '0'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}
		
		if (!empty($data['filter_full_name'])) {
			$sql .= " AND full_name LIKE '%" . $this->db->escape($data['filter_full_name']) . "%'";
		}
		
		if (!empty($data['filter_city'])) {
			$sql .= " AND city LIKE '%" . $this->db->escape($data['filter_city']) . "%'";
		}
		
		if (!empty($data['filter_post_code'])) {
			$sql .= " AND post_code LIKE '%" . $this->db->escape($data['filter_post_code']) . "%'";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getTotalOutlet() {
      	$sql = "SELECT COUNT(*) AS total FROM `outlet_request`";

		if (isset($data['filter_id_request']) && !is_null($data['filter_id_request'])) {
			$sql .= " WHERE id_request = '" . (int)$data['filter_id_request'] . "'";
		} else {
			$sql .= " WHERE id_request > '0'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}
		
		if (!empty($data['filter_full_name'])) {
			$sql .= " AND full_name LIKE '%" . $this->db->escape($data['filter_full_name']) . "%'";
		}
		
		if (!empty($data['filter_city'])) {
			$sql .= " AND city LIKE '%" . $this->db->escape($data['filter_city']) . "%'";
		}
		
		if (!empty($data['filter_post_code'])) {
			$sql .= " AND post_code LIKE '%" . $this->db->escape($data['filter_post_code']) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	
	public function addOutlet($data) {
      	$this->db->query("INSERT INTO `outlet_request` SET email = '" . $this->db->escape($data['email']) . "', full_name = '" . $this->db->escape($data['full_name']) . "', city = '" . $this->db->escape($data['city']) . "', post_code = '" . $this->db->escape($data['post_code']) . "' ");
      	$order_id = $this->db->getLastId();
	}
	
	public function getOutletRequest($id) {
		$order_query = $this->db->query("SELECT * from outlet_request WHERE id_request = '" . (int)$id . "'");

		if ($order_query->num_rows) {
            
            return array(
                'id_request'=>$order_query->row['id_request'],
                'email'=>$order_query->row['email'],
                'full_name'=>$order_query->row['full_name'],
                'city'=>$order_query->row['city'],
                'post_code'=>$order_query->row['post_code'],
			);
		} else {
			return false;
		}
	}
	/*
	public function getOutletRequest($id) {
		$query = $this->db->query("SELECT * FROM outlet_request WHERE id_request = '" . (int)$id . "'");
		
		return $query->rows;
	}
	*/
	public function deleteOutlet($id) {
		$this->db->query("DELETE FROM `outlet_request` WHERE id_request = '" . (int)$id . "'");
	}
	
	public function editOutlet($id, $data) {		 
		$this->db->query("UPDATE `outlet_request` SET email = '" . $this->db->escape($data['email']) . "', full_name = '" . $this->db->escape($data['full_name']) . "', city = '" . $this->db->escape($data['city']) . "', post_code = '" . $this->db->escape($data['post_code']) . "' WHERE id_request = '" . (int)$id . "'"); 
	}
	public function getTotalCustomersByEmail($email) {
		//$query = $this->db->query("SELECT COUNT(*) AS total FROM outlet_request WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		$query = $this->db->query("SELECT DISTINCT * FROM `outlet_request` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		//print_r($query);die();
		//return $query->row['total'];
		return $query->row;
	}
}
?>