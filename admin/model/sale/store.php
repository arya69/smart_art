<?php
class ModelSaleStore extends Model {
	public function getStore($data = array()) {
		$sql = "SELECT * FROM `outlet_data`";
		
		if (isset($data['filter_id_store']) && !is_null($data['filter_id_store'])) {
			$sql .= " WHERE id_store = '" . (int)$data['filter_id_store'] . "'";
		} else {
			$sql .= " WHERE id_store > '0'";
		}

		if (!empty($data['filter_city_provinces'])) {
			$sql .= " AND city_provinces LIKE '%" . $this->db->escape($data['filter_city_provinces']) . "%'";
		}
		
		if (!empty($data['filter_store'])) {
			$sql .= " AND store LIKE '%" . $this->db->escape($data['filter_store']) . "%'";
		}
		
		if (!empty($data['filter_phone'])) {
			$sql .= " AND phone LIKE '%" . $this->db->escape($data['filter_phone']) . "%'";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getTotalStore() {
      	$sql = "SELECT COUNT(*) AS total FROM `outlet_data`";

		if (isset($data['filter_id_store']) && !is_null($data['filter_id_store'])) {
			$sql .= " WHERE id_store = '" . (int)$data['filter_id_store'] . "'";
		} else {
			$sql .= " WHERE id_store > '0'";
		}

		if (!empty($data['filter_city_provinces'])) {
			$sql .= " AND city_provinces LIKE '%" . $this->db->escape($data['filter_city_provinces']) . "%'";
		}
		
		if (!empty($data['filter_store'])) {
			$sql .= " AND store LIKE '%" . $this->db->escape($data['filter_store']) . "%'";
		}
		
		if (!empty($data['filter_phone'])) {
			$sql .= " AND phone LIKE '%" . $this->db->escape($data['filter_phone']) . "%'";
		}
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	
	public function addStore($data) {
      	$this->db->query("INSERT INTO `outlet_data` SET city_provinces = '" . $this->db->escape($data['city_provinces']) . "', store = '" . $this->db->escape($data['store']) . "', phone = '" . $this->db->escape($data['phone']) . "' ");
      	$order_id_store = $this->db->getLastId();
	}
	
	public function getStoreRequest($id_store) {
		$order_query = $this->db->query("SELECT * from `outlet_data` WHERE id_store = '" . (int)$id_store . "'");

		if ($order_query->num_rows) {
            
            return array(
                'id_store'=>$order_query->row['id_store'],
                'city_provinces'=>$order_query->row['city_provinces'],
                'store'=>$order_query->row['store'],
                'phone'=>$order_query->row['phone'],
            );
		} else {
			return false;
		}
	}
	
	/*
	public function getOutletRequest($id_store) {
		$query = $this->db->query("SELECT * FROM outlet_data WHERE id_store = '" . (int)$id_store . "'");
		
		return $query->rows;
	}
	*/

	public function getTotalCustomersBycity_provinces($city_provinces) {
		//$query = $this->db->query("SELECT COUNT(*) AS total FROM `outlet_data` WHERE LOWER(city_provinces) = '" . $this->db->escape(utf8_strtolower($city_provinces)) . "'");
		$query = $this->db->query("SELECT DISTINCT * FROM `outlet_data` WHERE LCASE(city_provinces) = '" . $this->db->escape(utf8_strtolower($city_provinces)) . "'");
		
		//return $query->row['total'];
		return $query->row;
	}

	public function deleteStore($id_store) {
		$this->db->query("DELETE FROM `outlet_data` WHERE id_store = '" . (int)$id_store . "'");
	}
	
	public function editStore($id_store, $data) {		 
		$this->db->query("UPDATE `outlet_data` SET city_provinces = '" . $this->db->escape($data['city_provinces']) . "', store = '" . $this->db->escape($data['store']) . "', phone = '" . $this->db->escape($data['phone']) . "' WHERE id_store = '" . (int)$id_store . "'"); 
	}
	
}
?>