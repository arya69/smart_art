<?php
// HTTP
define('HTTP_SERVER', 'http://red.infomesta.net/~smartart/cart/admin/');
define('HTTP_CATALOG', 'http://red.infomesta.net/~smartart/cart/');

// HTTPS
define('HTTPS_SERVER', 'http://red.infomesta.net/~smartart/cart/admin/');
define('HTTPS_CATALOG', 'http://red.infomesta.net/~smartart/cart/');

// DIR
define('DIR_APPLICATION', '/home/smartart/public_html/cart/admin/');
define('DIR_SYSTEM', '/home/smartart/public_html/cart/system/');
define('DIR_DATABASE', '/home/smartart/public_html/cart/system/database/');
define('DIR_LANGUAGE', '/home/smartart/public_html/cart/admin/language/');
define('DIR_TEMPLATE', '/home/smartart/public_html/cart/admin/view/template/');
define('DIR_CONFIG', '/home/smartart/public_html/cart/system/config/');
define('DIR_IMAGE', '/home/smartart/public_html/cart/image/');
define('DIR_CACHE', '/home/smartart/public_html/cart/system/cache/');
define('DIR_DOWNLOAD', '/home/smartart/public_html/cart/download/');
define('DIR_LOGS', '/home/smartart/public_html/cart/system/logs/');
define('DIR_CATALOG', '/home/smartart/public_html/cart/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'smartart_ocdata');
define('DB_PASSWORD', '@alamaya_a25');
define('DB_DATABASE', 'smartart_ocdb');
define('DB_PREFIX', 'oc_');
?>